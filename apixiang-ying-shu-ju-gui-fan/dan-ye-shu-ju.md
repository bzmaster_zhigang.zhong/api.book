#单页数据
##JSON

参考
```json
{
    "code": "value",             //响应状态码，必需。客户端应首先根据此项结果进行相应处理。
    "message":"value",           //状态信息
    "data": {                    //返回数据
        "result1":
            {...},
        "result2":
            {...},
            ......
        
    },
    "timestamp": 1466152833      //服务器时间戳
}
```

示例
```json
{
  "code": 1,
  "message": "获取成功",
  "data": {
    "MdmBuMstr": {
      "BU_NAME": "345",
      "BU_SHORT_NAME": null,
      "BU_REGISTERED_NAME": null,
      "BU_LOGO_URL": null,
      "BU_PHONE": null,
      "BU_TYPE": 6,
      "BU_AREA": null,
      "STATION_COUNT": null,
      "BU_BRAND": null,
      "OPEN_DATE": null,
      "BU_ADDR_PROVINCE": null,
      "BU_ADDR_CITY": null,
      "BU_ADDR_DISTRICT": null,
      "BU_ADDR": null,
      "MNEMONIC_CODE": null,
      "BU_STATUS": 1,
      "BU_LEVEL": 0,
      "PARENT_BU_NO": null,
      "CREATE_PSN": 114,
      "CREATE_DATE": "2017-11-27 14:22:33",
      "UPDATE_PSN": 114,
      "UPDATE_DATE": "2017-11-27 14:22:33",
      "CREATE_ORG_NO": "BZM001",
      "DEL_FLAG": 1,
      "UDF10": null,
      "UDF1": null,
      "UDF2": null,
      "UDF3": null,
      "UDF4": null,
      "UDF5": null,
      "UDF6": null,
      "UDF7": null,
      "UDF8": null,
      "UDF9": null,
      "BRAND_LIST": null,
      "OC_NO": null,
      "OC_NAME": null,
      "BG_NO": null,
      "BG_NAME": null,
      "EDITION_ID": null,
      "BIZ_PARENT_BU_NOS": null,
      "BU_NO": "454",
      "FILE_CONTENT": null,
      "Id": "454"
    },
    "MapDat": []
  },
  "timestamp": 1511936856
}
```