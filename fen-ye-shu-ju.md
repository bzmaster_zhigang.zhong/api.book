#分页数据

##JSON

参考
```json
{
    "code": 1,                          //响应状态码，必需。客户端应首先根据此项结果进行相应处理。
    "message": "获取分页列表数据成功",    //状态信息
    "data": {                           //返回数据
        "Result": [
        {},
        ......
        ],
      "Page": {
        "PageIndex": 1,        //页数
        "PageSize": 2,         //每页记录数
        "TotalCount": 20,      //总行数
        "PageCount": 10,       //总页数
        "HasNext": true        //是否有下一页
      }
    },
    "timestamp": 1468468020    //服务器时间戳
}
```
示例
```json
{
  "code": 1,
  "message": "获取分页列表数据成功",
  "data": {
    "Result": [
      {
        "USR_ID": 722,
        "USR_TYPE": 0,
        "USR_NAME": "gdg",
        "USR_PWD_TOKEN": "EVNRrZ",
        "USR_PWD": "8becec1096109e9f0bac1ee6acd77466",
        "USR_MOBILE": "15300217281",
        "USR_EMAIL": "",
        "USR_MOBILE_PASS": null,
        "USR_STATUS": 1,
        "USR_AVATAR_STATUS": null,
        "USR_REG_DATE": "2017-11-24 09:41:51",
        "USR_REAL_NAME": "郭德纲",
        "USR_AVATAR_PATH": "",
        "USR_ALIPAY": "",
        "ORG_NO": "BZM001",
        "INVITATION_CODE": "",
        "FROM_CHANNEL": "",
        "FROM_INVITATION_CODE": "",
        "USR_FIELD": "",
        "USR_TITLE": "",
        "COMPANY_CODE": "",
        "COMPANY_NAME": "上海比滋特信息技术有限公司",
        "EMPLOYEE_NO": "",
        "USR_DEPT_NAME": "",
        "USR_DEPT_NO": "",
        "ERP_USR_ID": "",
        "USR_CONTRACT_ADDR": "",
        "USR_WORK_ADDR": "",
        "USR_BIRTHDAY": null,
        "ACTIVITY_REGION": "",
        "USR_ACTIVITY": "",
        "UDF1": "",
        "UDF2": "",
        "UDF3": "",
        "UDF4": "",
        "UDF5": "",
        "USR_NICKNAME": "",
        "USR_REGION": "",
        "USR_CURRENT_STATUS": null,
        "BEAT_DATE": null,
        "DEL_FLAG": 1,
        "LOGIN_DATE": null,
        "LOGOUT_DATE": null,
        "CREATE_PSN": 114,
        "CREATE_DATE": "2017-11-24 09:41:51",
        "UPDATE_PSN": 114,
        "UPDATE_DATE": "2017-11-24 09:41:51",
        "CREATE_ORG_NO": "BZM001",
        "ATTN_GPS": "",
        "REG_ID": "1114a8979297c5b4d0a",
        "DEPT_ID": "",
        "REG_PLATFORM": "ios"
      },
      {
        "USR_ID": 717,
        "USR_TYPE": 0,
        "USR_NAME": "ceshiwe",
        "USR_PWD_TOKEN": "5FCFWj",
        "USR_PWD": "1ce3a9bb78e520fee2eb06fe6b2581ad",
        "USR_MOBILE": "13845454455",
        "USR_EMAIL": "",
        "USR_MOBILE_PASS": 0,
        "USR_STATUS": 1,
        "USR_AVATAR_STATUS": 0,
        "USR_REG_DATE": "2017-11-21 23:21:01",
        "USR_REAL_NAME": "伊娃",
        "USR_AVATAR_PATH": "",
        "USR_ALIPAY": "",
        "ORG_NO": "BZM001",
        "INVITATION_CODE": "",
        "FROM_CHANNEL": "后台导入",
        "FROM_INVITATION_CODE": "",
        "USR_FIELD": "",
        "USR_TITLE": "",
        "COMPANY_CODE": "BZM001",
        "COMPANY_NAME": "上海比滋特信息技术有限公司",
        "EMPLOYEE_NO": "",
        "USR_DEPT_NAME": "",
        "USR_DEPT_NO": "",
        "ERP_USR_ID": "",
        "USR_CONTRACT_ADDR": "",
        "USR_WORK_ADDR": "",
        "USR_BIRTHDAY": "1993-03-23 00:00:00",
        "ACTIVITY_REGION": "",
        "USR_ACTIVITY": "",
        "UDF1": "",
        "UDF2": "",
        "UDF3": "",
        "UDF4": "",
        "UDF5": "",
        "USR_NICKNAME": "",
        "USR_REGION": "",
        "USR_CURRENT_STATUS": 0,
        "BEAT_DATE": null,
        "DEL_FLAG": 1,
        "LOGIN_DATE": null,
        "LOGOUT_DATE": null,
        "CREATE_PSN": 114,
        "CREATE_DATE": "2017-11-21 23:21:01",
        "UPDATE_PSN": 114,
        "UPDATE_DATE": "2017-11-21 23:21:01",
        "CREATE_ORG_NO": "BZM001",
        "ATTN_GPS": "",
        "REG_ID": "",
        "DEPT_ID": "",
        "REG_PLATFORM": ""
      }
    ],
    "Page": {
      "PageIndex": 1,
      "PageSize": 2,
      "TotalCount": 20,
      "PageCount": 10,
      "HasNext": true
    }
  },
  "timestamp": 1511935896
}
```