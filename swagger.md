`Swagger`是一个开源的接口文档管理工具，如果你需要了解接口一些详细信息，或测试接口的正确性，你可以使用`Swagger`。

![swagger](/assets/swagger.png)

如何测试接口，你需要先使用`Postman`工具或其它类似工具先获取有效的令牌
再如图所示，做一次令牌绑定的操作，你就可以开始测试了
![swagger](/assets/swagger_authorize.png)