#列表数据
##JSON

参考
```json
{
    "code": "value",       //响应结果码，必需。客户端应首先根据此项结果进行相应处理。
    "message":"value",     //状态信息
    "data": [              //返回数据
        {...},
        ......
    ],
    "timestamp":1468485027  //服务器时间戳
} 
```

示例

```json
{
  "code": 1,
  "message": "获取成功",
  "data": [
    {
      "BU_NO": "SHBSJT",
      "BU_NAME": "上海布氏集团"
    },
    {
      "BU_NO": "DFGETG",
      "BU_NAME": "测试集团"
    }
  ],
  "timestamp": 1511937051
}
```