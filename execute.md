#添加、更新、删除
##JSON

参考

```json
{
    "code": "value",             //响应状态码，必需。客户端应首先根据此项结果进行相应处理。
    "message":"value",           //状态信息
    "data": null,                //返回数据
    "timestamp": 1466152833      //服务器时间戳
}
``` 
示例

```json
{
  "code": 1,
  "message": "操作成功",
  "data": {
    "SO_NO": "IO1709200319330000002",
    "BIZ_TYPE": null,
    "SO_TYPE": null,
    "CUS_NO": "",
    "CUS_NAME": "测试",    
    "SO_STATUS_DESC": "已完成",
    "PAYMENT_STATUS_DESC": "",
    "Id": "5bcba7ef-8d3e-4a5e-b84f-7b89e74fa25e"
  },
  "timestamp": 1511936659
}
```