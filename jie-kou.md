如果你访问的接口是需要授权的并且你已经持有有效的令牌，你可以开始请求接口了。

针对于需要验证`Token`的接口，每次请求时你都需要在头部信息`Heades`中添加下面的参数，这是必须的
```md
Key：    Authorization
Value：  Bearer+空格+access_token
```
请求后台的接口时还需要的传入头部Timestamp、Sign头部信息
```md
Timestamp=1466152833 /*时间戳*/
Sign=A2BF968D11DFE222128A212A6EBACD9B/*数字签名*/
```
JS示例
```js
export default async(url = '', data = {}, type = 'GET', method = 'fetch') => {
    var requestParameterString = JSON.stringify(data);
    var appKey = "9zk6ZMQMSUYq16xqxYP8cQ=="; 
    var authToken = Authorization;/*当前的token，没有则为空字符串*/
    var timestamp = Date.parse(new Date()) / 1000;
    var hash = md5(requestParameterString + appKey + timestamp + authToken).toUpperCase();//MD5加密后转大写
    obj.headers["Timestamp"] = timestamp;
    obj.headers["Sign"] = hash;
}
```

成功将返回接口处理结果，如果令牌失效，你调用接口时，接口会返回`401`状态码，其它接口错误，这里不一一列举

如图所示：
![刷新令牌](/assets/api_test.png)