# Summary

## APP接口使用指南

* [开始](README.md)
* [令牌](chapter1.md)
* [接口](jie-kou.md)
* [Swagger](swagger.md)

## APP接口开发规范

* [介绍](appjie-kou-kai-fa-gui-fan/jie-shao.md)
* [状态码规范](zhuang-tai-ma-gui-fan.md)
* [接口编写规范](jie-kou-bian-xie-gui-fan.md)
* [API响应数据规范](apixiang-ying-shu-ju-gui-fan.md)
  * [单页数据](apixiang-ying-shu-ju-gui-fan/dan-ye-shu-ju.md)
  * [列表数据](lie-biao-shu-ju.md)
  * [分页数据](fen-ye-shu-ju.md)
  * [Execute\(CUD\)](execute.md)
  * [错误信息](cuo-wu-xin-xi.md)

