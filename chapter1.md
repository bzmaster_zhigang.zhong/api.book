####1.登录获取令牌

这里我将使用Postman来演示如何获取一个令牌,生产环境中 你需要将[http://localhost:5000](http://localhost:5000) 替换成生产环境的域名。

请求内容`Request Content`，你需要传入一些参数

```md
username：        用户名
password：        用户密码
grant_type:       授权模式，目前采用password（密码模式）
client_id:        客户端标识，app采用password_client
client_secret:    客户端识别密钥，app 统一采用 secret
```

成功：响应内容`Response Content`将返回

```md
access_token:      令牌
expires_in:        时效（过期时间，目前1个小时令牌将会失效，你需要重新获取令牌）
token_type:        令牌类型
refresh_token:     刷新令牌(用来刷新令牌)
```
失败：你将接收到状态码为`400`的响应 并看到如下几种返回结果

1.不支持的授权模式，请检查`grant_type` 参数是否有误
```json
{
    "error": "unsupported_grant_type"
}
```
2.无效的客户端，请检查`client_id`和`client_secret` 是否有误
```json
{
    "error": "invalid_client"
}
```
3.无效的授权，请检查用户名或密码是否有误
```json 
{
    "error": "invalid_grant"
}
```

如图所示：
![获取令牌成功](/assets/gettoken.png)

####2.刷新令牌

请求接口后如果返回状态码为`401(Unauthorized)`则表示令牌是无效的，你需要重新获取令牌，你可以使用刷新令牌的方式去获取一个新的令牌，当然你也可以使用登录获取令牌的方式生成新的令牌

请求内容`Request Content`，你需要传入几个参数
```md
grant_type:        授权模式，如果你是要刷新令牌，请填入refresh_token
refresh_token:     刷新令牌
client_id:         客户端标识，app采用password_client
client_secret:     客户端识别密钥，app 统一采用 secret
```

成功：响应内容`Response Content`将返回
```md
access_token:        令牌
expires_in:          时效（过期时间，目前1个小时令牌将会失效，你需要重新获取令牌）
token_type:          令牌类型
refresh_token:       刷新令牌(用来刷新令牌)
```

失败：你将接收到状态码为`400`的响应 并看到如下几种返回结果

1.不支持的授权模式，请检查`grant_type` 参数是否有误
```json
{
    "error": "unsupported_grant_type"
}
```
2.无效的客户端，请检查`client_id`和`client_secret` 是否有误
```json
{
    "error": "invalid_client"
}
```
3.无效的授权，请检查刷新令牌是否有误
```json 
{
    "error": "invalid_grant"
}
```
4.无效请求，请检查是否缺少必需的参数
```json
{
    "error": "invalid_request"
}
```

如图所示：
![获取令牌失败](/assets/refresh_token.png)






